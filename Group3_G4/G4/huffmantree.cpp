﻿#include "huffmantree.h"
/*  từ đầu t đã nói là không nên để hàm này ở đây
	vì cái này đồng nghĩa với mỗi lần tạo cây đều phải sort lại
	chuyện này là vô cùng không cần thiết
	nên sort ngay từ trước khi gửi vào hàm và lưu lại bảng đã sort*/
bool dontNeedSwap(CharStat a, CharStat b)
{
	if (a.frequency == b.frequency)
	{
		return a.name > b.name;
	}
	return a.frequency > b.frequency;
}

/***************************************************************
*	Tên hàm		:	HuffmanTree
*	Chức năng	:	dựa vào bảng tần số ký tự để tạo cây Huffman
*	Truyền vào	:	bắt buộc là bảng thống kê đã sắp xếp trọng số
*					(nếu trọng số bằng nhau sẽ ưu tiên ký tự có mã
*					ASCII lớn hơn nằm trước)
*	Trả về		:	cây Huffman với root ở vị trí đầu tiên i = 0
*					các phần tử lá sẽ có nLeft và nRight bằng -1
*	Người viết	:	Đạt
***************************************************************/
HuffNode* HuffmanTree(vector<CharStat> stat)
{
	//nếu stat không có ký tự nào
	if (stat.size() == 0) 
		return NULL;

	//tạo cây Huffman
	HuffNode *HuffTree = new HuffNode[stat.size() * 2 - 1];

	//nếu chỉ có duy nhất 1 ký tự
	if (stat.size() == 1) {
		HuffTree[0].c = HuffTree[1].c = stat[0].name;
		HuffTree[0].nFreq = HuffTree[1].nFreq = stat[0].frequency;
		HuffTree[0].nLeft = 1;
		HuffTree[0].nRight = -1;
		HuffTree[1].nLeft = HuffTree[1].nRight = -1;
		return HuffTree;
	}

	int Offset = 1, i, j;
	bool isAdded;
	int Pos[3];
	CharStat Insert;

	//sắp xếp lại bảng thống kê
	sort(stat.begin(), stat.end(), dontNeedSwap);

	while (true)
	{
		//chọn 2 phần tử xuất hiện ít nhất (2 phần tử cuối ở bảng)
		//để thêm vào cây
		for (j = 1; j <= 2; j++)
		{
			//xét xem phần tử đang xét đã được thêm vào cây hay chưa
			for (i = 0, isAdded = false; i < Offset; i++)
			{
				if (HuffTree[i].c == (stat.end() - j)->name)
				if (HuffTree[i].nFreq == (stat.end() - j)->frequency)
				{
					isAdded = true;
					Pos[j] = i;				//ghi nhớ vị trí đã thêm vào
					break;
				}
			}
			//nếu phần tử đang xét chưa được thêm vào cây
			//thêm vào cây
			if (!isAdded)
			{
				HuffTree[Offset].c = (stat.end() - j)->name;
				HuffTree[Offset].nFreq = (stat.end() - j)->frequency;
				HuffTree[Offset].nLeft = HuffTree[Offset].nRight = -1;
				Pos[j] = Offset;			//ghi nhớ vị trí đã thêm vào
				Offset++;
			}
		}

		//thêm nút cha vào cây
		if (stat.size() == 2)
			Offset = 0;
		HuffTree[Offset].c = (stat.end() - 1)->name;
		HuffTree[Offset].nFreq = (stat.end() - 1)->frequency + (stat.end() - 2)->frequency;
		HuffTree[Offset].nLeft = Pos[1];
		HuffTree[Offset].nRight = Pos[2];
		Offset++;
		if (stat.size() == 2)
			break;

		//loại bỏ 2 phần tử ra khỏi bảng
		stat.erase(stat.end() - 1);
		stat.erase(stat.end() - 1);

		//thêm nút cha vào bảng
		Insert.name = HuffTree[Offset - 1].c;
		Insert.frequency = HuffTree[Offset - 1].nFreq;
		vector<CharStat>::iterator it = stat.begin();
		for (i = 0; it != stat.end(); it++, i++)
		{
			//nếu 2 nút có trọng số bằng nhau
			if (it->frequency == Insert.frequency)
			{
				//nếu nút cần thêm có mã ACSII nhỏ nhất
				//thì thêm vào bảng
				if (Insert.name > it->name)
				{
					stat.insert(it, Insert);
					break;
				}
			}
			//nếu nút cần thêm có trọng số nhỏ hơn
			//thì thêm vào bảng
			else if (Insert.frequency > it->frequency)
			{
				stat.insert(it, Insert);
				break;
			}
		}
		//khi lượt hết cả dãy mà không tìm được vị trí thêm
		//tức là trọng số của các node hiện có trong dãy đều lớn hơn trọng số node cần thêm vào
		//thì ta thêm node này vào cuối dãy
		if (i == stat.size())
		{
			stat.insert(it, Insert);
		}
	}

	return HuffTree;
}

/***************************************************************
*	Tên hàm		:	generateCodeList
*	Chức năng	:	dựa vào cây Huffman để tạo một danh sách mã
*					bit tương ứng với các ký tự
*	Truyền vào	:	cây Huffman, con trỏ tới
*					danh sách mã bit cần tạo
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void generateCodeList(HuffNode * root, int cur, unsigned char tempcode[32],
	int top, symbol *code_list) {
	/* thêm bit 0 vào code tạm, rẽ trái */
	if (root[cur].nLeft != -1) {
		clearBit(tempcode, top); /* Tắt bit ở vị trí top của code tạm */
		generateCodeList(root, root[cur].nLeft, tempcode, top + 1, code_list); /* Tăng top thêm 1, rẽ trái */
	}
	/* thêm bit 1 vào code tạm, rẽ phải */
	if (root[cur].nRight != -1) {
		setBit(tempcode, top); /* Bật bit ở vị trí top của code tạm */
		generateCodeList(root, root[cur].nRight, tempcode, top + 1, code_list); /* Tăng top thêm 1, rẽ phải */
	}
	/* nếu gặp node lá */
	if ((root[cur].nLeft == -1) && (root[cur].nRight == -1)) {
		code_list[root[cur].c].length = top; /* Cập nhật độ dài code của ký tự c */
		copyBlock(code_list[root[cur].c].code, tempcode); /* Copy code tạm vào code của ký tự c trong bảng mã */
	} 
}

/***************************************************************
*	Tên hàm		:	printCodeList
*	Chức năng	:	in bảng mã ra stdout để phục vụ cho việc 
*					debug
*	Truyền vào	:	bảng mã
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void printCodeList(symbol *code_list) {
	for (int i = 0; i < NUM_CHARS; i++) {
		cout << "Character:\t" << i << "\t| Code length:\t" << code_list[i].length << "\t| Code:\t";
		for (int j = 0; j < code_list[i].length; j++)
			cout << testBit(code_list[i].code, j);
		cout << endl;
	}
}
void gotoxy(int x, int y)
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD c = { x, y };
	SetConsoleCursorPosition(h, c);
}

COORD GetCurrenConsoleCursorPosition()
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbInfo;
	GetConsoleScreenBufferInfo(h, &csbInfo);
	return csbInfo.dwCursorPosition;
}

/***************************************************************
*	Tên hàm		:	compressFile
*	Chức năng	:	dịch đoạn bit gốc của file thành đoạn bit nén
*					Huffman, sau đó ghi đoạn bit này ra file đích
*					đồng thời cập nhật số bit align
*	Truyền vào	:	cây Huffman, file cần nén,
*					file nén, số bit align
*	Trả về		:	kích thước file sau khi nén
*	Người viết	:	Duy
***************************************************************/
unsigned int CompressFile(ifstream& file, ofstream& fileCompress,
	HuffNode* root, char& Align, unsigned int size) {
	symbol code_list[NUM_CHARS];

	for (int i = 0; i < NUM_CHARS; i++) {
		code_list[i].length = -1;
		for (int j = 0; j < 32; j++) {
			code_list[i].code[j] = 1;
		}
	}

	int i;
	char c;
	int bit_count, byte_count;
	char bit_buffer;

	/* Reset số bit align */
	Align = 0;

	unsigned char tempcode[32];
	generateCodeList(root, 0, tempcode, 0, code_list); /* Tạo bảng mã */
	/*freopen("code list.txt", "w", stdout);
	printCodeList(code_list);
	fclose(stdout);*/

	bit_buffer = 0;
	bit_count = 0;
	byte_count = 0;
	int readByteCount = 0;
	COORD current = GetCurrenConsoleCursorPosition();

	/* Ghi các mã bit đã encode ra file từng byte một */
	while (file.read(&c, 1)) {
		readByteCount++;
		if ((readByteCount % (size / 100)) == 0)
		{
			gotoxy(current.X, current.Y);
			cout << "Tien do: [" << (readByteCount / (size / 100)) << "%]";
		}
		int tmp = c & 0xff;
		for (i = 0; i < code_list[tmp].length; i++) {
			bit_count++;
			/* Ghép các bit từ danh sách mã bit nối tiếp nhau để lấp đầy buffer */
			bit_buffer = (bit_buffer << 1) | (testBit(code_list[tmp].code, i) == 1);
			if (bit_count == 8) {
				/* Ngay khi buffer vừa đầy thì ghi buffer ra file */
				fileCompress << bit_buffer;
				byte_count++;
				bit_count = 0;
			}
		}
	}
	/* Số bit trong buffer không đủ 1 byte */
	/* Ghi các bit còn lại, bao gồm luôn 1 số bit align */
	if (bit_count != 0) {
		Align = 8 - bit_count;
		bit_buffer <<= 8 - bit_count;
		fileCompress.write(&bit_buffer, 1);
		byte_count++;
	}
	gotoxy(current.X, current.Y);
	cout << "[ OK ] Da nen xong.     " << endl;
	return byte_count;
}

/***************************************************************
*	Tên hàm		:	ExtractFile
*	Chức năng	:	dựa vào cây Huffman để dịch đoạn bit đã nén
*					trong file nén để giải mã, sau đó ghi đoạn
*					bit đã được giải mã ra file đích
*	Truyền vào	:	con trỏ file đích, con trỏ file nén, cây 
*					Huffman, số lượng bit align
*	Trả về		:	kích thước file sau khi giải nén
*	Người viết	:	Anh
***************************************************************/
unsigned int ExtractFile(ofstream& file, ifstream& fileCompress, HuffNode* root, char Align, unsigned int size)
{
	char tempByte;
	int bitCount;
	unsigned int byteCount;
	int bit;
	HuffNode pCurr = root[0];
	// Khởi tạo byteCount = 0, nếu ghi thành công 1 byte sẽ tăng lên 1
	byteCount = 0; unsigned int i = 0;
	COORD current = GetCurrenConsoleCursorPosition();
	// Đọc từng byte trong fileCompress cho đến hết
	while (i < size)
	{
		fileCompress.read(&tempByte, sizeof(tempByte)); i++;
		if ((i % (size / 100)) == 0)
		{
			gotoxy(current.X, current.Y);
			cout << "Tien do: [" << (i / (size / 100)) << "%]";
		}
		// Khởi tạo bitCount = 0, dùng để đếm thứ tự bit để lấy ra trong tempByte từ phải sang trái
		bitCount = 0;
		// Lặp đến khi đọc đủ 8 bit của 1 block, hoặc đến khi đọc đủ đến số bit đã ghi, bỏ qua bit thừa.
		do
		{
			bitCount++;
			bit = (tempByte >> (8 - bitCount)) & 0x01;
			// Nếu bit == 0, đi tiếp sang nhánh trái, ngược lại thì sang nhánh phải
			if (bit == 0) { pCurr = root[pCurr.nLeft]; }
			else { pCurr = root[pCurr.nRight]; }
			// Nếu node hiện tại là node lá thì xuất ký tự đó ra file, node hiện tại trở về nhận node gốc, tăng byteCount lên 1
			if ((pCurr.nLeft == -1) && (pCurr.nRight == -1))
			{
				char tempChar = static_cast<char>(pCurr.c);
				file.write(&tempChar, sizeof(tempChar));
				byteCount++;
				pCurr = root[0];
			}

		} while (((i < size) && (bitCount < 8)) || ((i == size) && (bitCount < (8 - Align))));
	}
	gotoxy(current.X, current.Y);
	cout << "[ OK ] Da giai nen xong.    " << endl;

	return byteCount;
}

/***************************************************************
*	Tên hàm		:	Checksum
*	Chức năng	:	kiểm tra file trước và sau giải nén có cùng
*                   kích thước hay ko
*	Truyền vào	:	size trước và sau giải nén
*	Trả về		:	0-ko thành công, 1-thành công
*	Người viết	:	Dũng
***************************************************************/
bool Checksum(unsigned int Size, unsigned int sizeBefore)
{
	return Size == sizeBefore;
}