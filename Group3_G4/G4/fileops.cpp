﻿#include "fileops.h"

/***************************************************************
*	Tên hàm		:	fileList
*	Chức năng	:	duyệt folder và trả về danh sách chứa tên và
*                   đường dẫn tới các file có trong thư mục
*	Truyền vào	:	địa chỉ folder, vector trả về chứa kích thước
*                   từng tập tin, vector chuỗi chứa danh sách tên
*	Trả về		:	duyệt thành công hay không
*	Người viết	:	Dũng
***************************************************************/
bool fileList(TCHAR* directory, vector<unsigned long>& bytes, vector<string>& filenames) {
	//struct store file attributes
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize; //64 bits signed integer
	TCHAR folder[MAX_PATH];

	//handle file
	HANDLE hFind = INVALID_HANDLE_VALUE;
	size_t length_of_arg;

	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.
	StringCchLength(directory, MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3)) {
		return false;
	}

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.
	StringCchCopy(folder, MAX_PATH, directory);
	StringCchCat(folder, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.
	hFind = FindFirstFile(folder, &ffd);

	//if there is no file in the folder
	if (INVALID_HANDLE_VALUE == hFind) {
		return false;
	}

	do {
		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
			//find each file size
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;

			//push name and size to respective vector
			bytes.push_back(filesize.QuadPart);
			string fileName(directory);
			fileName = fileName + '\\';
			fileName = fileName + ffd.cFileName;
			filenames.push_back(fileName);
		}

	} while (FindNextFile(hFind, &ffd) != 0);

	if (filenames.size() == 0) {
		return false;
	}

	return true;
}

/***************************************************************
*	Tên hàm		:	CharFreq
*	Chức năng	:	tạo bảng tần số những kí tự xuất hiện trong
*                   các file
*	Truyền vào	:	vector chuỗi chứa danh sách tên
*	Trả về		:	vector chứa CharStat đã xếp thứ tự từ xuất hiện
*                   nhiều tới ít, nếu tần số bằng nhau thì xếp theo mã ASCII
*					từ thấp tới cao
*	Người viết	:	Dũng
***************************************************************/
void CharFreq(vector<string> p, vector<CharStat>& table)
{
	unsigned int stat[256];
	for (int i = 0; i < 256; i++)
		stat[i] = 0;
	int n = p.size();

	for (int i = 0; i < n; i++)
	{
		ifstream temp(p[i], ios::binary);
		if (!temp.is_open())
			continue;
		/*bool t = temp.eof();
		while (!t)
		{
		char tempChar;
		temp.read(&tempChar, 1);
		t = temp.eof();
		if (t) break;
		int j = tempChar&0xff;
		stat[j]++;
		}*/

		while (!temp.eof())
		{
			char tempChar[MAX];
			temp.read(tempChar, MAX);
			int t = temp.gcount();
			for (int i = 0; i < MAX; i++)
			{
				int j = tempChar[i] & 0xff;
				stat[j]++;
			}
			if (t<MAX) break;
		}

		temp.close();
	}
	for (int i = 0; i < 256; i++)
	{
		if (stat[i]>0)
		{
			CharStat s;
			s.name = i; s.frequency = stat[i];
			table.push_back(s);
		}
	}
}
