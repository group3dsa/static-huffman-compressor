﻿/**************************************************
Thư viện này chứa các hàm chức năng của
chương trình.
**************************************************/

#ifndef __APPOPS_H__
#define __APPOPS_H__

#include "fileops.h"
#include "huffile.h"
#include "huffmantree.h"
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

void Encode(char *folder_in, char* file_out);

void View(char* file);

bool Decode(char* func, char* file);

#endif                                                       