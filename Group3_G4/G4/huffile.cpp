﻿#include "huffile.h"

HUF::HUF(void)
{
	m_CodeFile[0] = 'G'; m_CodeFile[1] = '3';
	m_nCode = 0;
}

HUF::HUF(char * Code, int nCode, vector<CharStat> CodeTable)
{
	m_CodeFile[0] = Code[0]; m_CodeFile[1] = Code[1];
	m_nCode = nCode;
	m_CodeTable = CodeTable;
}

HUF::~HUF(void)
{
}

bool HUF::isHUF()
{
	return m_CodeFile[0] == 'G'&&m_CodeFile[1] == '3';
}

HUFHeader HUF::getHeader(int i)
{
	return m_FileHeader[i];
}

void HUF::insertHeader(HUFHeader t)
{
	m_FileHeader.push_back(t);
}

istream& operator>>(istream& in, HUF& info)
{
	if (in)
	{
		in.read(info.m_CodeFile, 2);
		if (in.eof()) return in;
		char temp;
		in.read(&temp, 1);
		if (in.eof()) return in;
		info.m_nCode = temp & 0x00ff;
		if (temp == 0) info.m_nCode = 256;
		for (int i = 0; i < info.m_nCode; i++)
		{
			CharStat tempCS;
			char tempT[5];
			in.read(tempT, 5);
			if (in.eof()) return in;
			tempCS.name = tempT[0];
			tempCS.frequency = 0;
			for (int i = 1; i < 5; i++)
			{
				tempCS.frequency = tempCS.frequency << 8;
				tempCS.frequency = tempCS.frequency | (tempT[i] & 0xff);
			}
			info.m_CodeTable.push_back(tempCS);
		}
		unsigned int saved = in.tellg();
		in.seekg(0, ios::end);
		unsigned int endFile = in.tellg();
		in.seekg(saved);
		HUFHeader tempHeader;
		do
		{
			bool t = tempHeader.Input(in);
			if (t == false) break;
			info.m_FileHeader.push_back(tempHeader);
			in.seekg(tempHeader.nextFileAddrOffset);
		} while (tempHeader.nextFileAddrOffset < endFile);
	}
	return in;
}

ostream& operator<<(ostream& out, HUF& info)
{
	if (out)
	{
		out.write(info.m_CodeFile, 2);
		char temp = info.m_nCode & 0xff;
		out.write(&temp, 1);
		for (int i = 0; i < info.m_nCode; i++)
		{
			char tempCS[5];
			tempCS[0] = info.m_CodeTable[i].name;
			tempCS[1] = info.m_CodeTable[i].frequency >> 24;
			tempCS[2] = info.m_CodeTable[i].frequency >> 16;
			tempCS[3] = info.m_CodeTable[i].frequency >> 8;
			tempCS[4] = info.m_CodeTable[i].frequency;
			out.write(tempCS, 5);
		}
	}
	return out;
}

bool HUFHeader::Input(istream& in)
{
	fileName = "";
	char temp;
	//temp = in.get();
	in.read(&temp, 1);
	if (in.eof()) return false;
	while (temp>7 || temp<0)
	{
		fileName += temp;
		//temp = in.get();
		in.read(&temp, 1);
		if (in.eof()) return false;
	}
	fileName += '\0';
	alignBits = temp;

	char tempChar[4];
	in.read(tempChar, 4);
	if (in.eof()) return false;
	sizeBefore = 0;
	for (int i = 0; i < 4; i++)
	{
		sizeBefore = sizeBefore << 8;
		sizeBefore = sizeBefore | (tempChar[i] & 0x0ff);
	}

	in.read(tempChar, 4);
	if (in.eof()) return false;
	nextFileAddrOffset = 0;
	for (int i = 0; i < 4; i++)
	{
		nextFileAddrOffset = nextFileAddrOffset << 8;
		nextFileAddrOffset = nextFileAddrOffset | (tempChar[i] & 0x0ff);
	}
	curFileAddrOffset = in.tellg() & 0xffffffff;
	if (curFileAddrOffset == -1) return false;
	sizeAfter = nextFileAddrOffset - curFileAddrOffset;
	return true;
}

bool HUFHeader::Output(ostream & out)
{
	int n = fileName.length();
	int i = 0;
	while (i<n - 1)
	{
		out.write(&fileName[i], 1);
		i++;
	}
	out.write(&alignBits, 1);

	char tempChar[4];
	tempChar[0] = sizeBefore >> 24 & 0xff;
	tempChar[1] = sizeBefore >> 16 & 0xff;
	tempChar[2] = sizeBefore >> 8 & 0xff;
	tempChar[3] = sizeBefore & 0xff;
	out.write(tempChar, 4);

	tempChar[0] = nextFileAddrOffset >> 24 & 0xff;
	tempChar[1] = nextFileAddrOffset >> 16 & 0xff;
	tempChar[2] = nextFileAddrOffset >> 8 & 0xff;
	tempChar[3] = nextFileAddrOffset & 0xff;
	out.write(tempChar, 4);
	return true;
}
