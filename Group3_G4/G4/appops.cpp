#include "appops.h"

void Encode(char * folder_in, char * file_out)
{
	ofstream out(file_out, ios::binary);
	if (out.is_open())
	{
		TCHAR *input = folder_in;
		vector<unsigned long> fileSizes;
		vector<string> p;
		//vector<fs::path> p = fileList(folder_in);
		if (fileList(input, fileSizes, p))
		{
			int k = p.size();
			if (k > 0)
			{
				vector<CharStat> stat;
				CharFreq(p, stat);
				int size = stat.size();
				if (size > 0)
				{
					HUF info("G3", size, stat);
					out << info;

					HuffNode* root = HuffmanTree(stat);
					//Nen tung file				
					for (int i = 0; i < k; i++)
					{
						HUFHeader infoFile;
						infoFile.curFileAddrOffset = out.tellp() & 0xffffffff;
						//infoFile.fileName = p[i].string();
						string temp;
						int iTemp = p[i].length();
						for (; iTemp>-1; iTemp--)
						{
							if (p[i][iTemp] == '\\') break;
						}
						iTemp++;
						for (; iTemp <= p[i].length(); iTemp++)
						{
							infoFile.fileName = infoFile.fileName + p[i][iTemp];
						}
						ifstream inFile(p[i], ios::ate | ios::binary);
						if (!inFile.is_open()) continue;
						infoFile.sizeBefore = (unsigned int)inFile.tellg();
						infoFile.alignBits = 0;
						infoFile.sizeAfter = 0;
						cout << "Nen file: " << infoFile.fileName << endl;
						infoFile.Output(out);
						inFile.seekg(0, ios::beg);
						infoFile.sizeAfter = CompressFile(inFile, out, root, infoFile.alignBits, infoFile.sizeBefore);


						infoFile.nextFileAddrOffset = out.tellp() & 0xffffffff;

						out.seekp(infoFile.curFileAddrOffset);

						infoFile.Output(out);
						//info.insertHeader(infoFile);

						out.seekp(infoFile.nextFileAddrOffset);
					}
					out.close();
					cout << "[ OK ] Nen file thanh cong!\n";
				}
				else
				{
					cout << "[ FAIL ] Nen file that bai!\n";
				}
			}
		}
		else
		{
			cout << "[ FAIL ] Khong mo duoc thu muc!\n";
		}
	}
	else
	{
		cout << "[ FAIL ] Khong tao duoc file!\n";
	}
}

void View(char * file)
{
	ifstream in(file, ios::binary);
	if (in)
	{
		HUF info;
		in >> info;
		if (info.isHUF())
		{
			vector<HUFHeader> stat = info.getHeaders();
			int n = stat.size();
			cout << left << setw(5) << "STT";
			cout << left << setw(35) << "Ten file";
			cout << left << setw(20) << "Size truoc nen";
			cout << left << setw(10) << "Size sau nen\n";
			for (int d = 0; d < 72; d++) cout << "_";
			cout << endl << endl;
			for (int i = 0; i < n; i++)
			{
				int file_count = i + 1;
				cout << right << setw(3) << file_count;
				cout << left << setw(2) << ". ";
				cout << left << setw(35) << stat[i].fileName;
				cout << left << setw(20) << (unsigned int)stat[i].sizeBefore;
				cout << left << setw(20) << (unsigned int)stat[i].sizeAfter;
			}
		}
		else
		{
			cout << "[ FAIL ] File khong dung dinh dang hoac corrupt!\n";
		}
	}
	else
	{
		cout << "[ FAIL ] Khong the mo file " << file << "!\n";
	}
}

bool Decode(char * func, char * file)
{
	if (func[0] == '-')
	{
		if (func[1] == 'a'&&func[2] == 'l'&&func[3] == 'l'&&func[4] == '\0')
		{
			ifstream in(file, ios::binary);
			if (!in) return false;
			HUF info;
			in >> info;
			vector<HUFHeader> headers = info.getHeaders();
			vector<CharStat> stat = info.getCharStat();
			if (stat.size() > 0)
			{
				//tao cay huffman
				HuffNode* root = HuffmanTree(stat);
				int n = headers.size();
				cout << "Bat dau giai nen tu file " << file << "...\n\n";
				for (int i = 0; i < n; i++)
				{
					ofstream out(headers[i].fileName, ios::binary);
					if (!out) return false;
					in.seekg(headers[i].curFileAddrOffset);
					cout << "Giai nen file: " << headers[i].fileName << endl;
					unsigned int Size = ExtractFile(out, in, root, headers[i].alignBits, headers[i].sizeAfter);
					bool check_sum = Checksum(Size, headers[i].sizeBefore);
					out.close();
					if (check_sum) cout << "[ OK ] Checksum file " << headers[i].fileName << "hop le.\n";
					else cout << "[ FAIL ] Checksum khong hop le. Giai nen file " << headers[i].fileName << "that bai!\n";
				}
			}
		}
		else if (func[1] == '[')
		{
			vector<int> n;
			int i = 2;
			while (func[i] != ']')
			{
				int getSelectedFile = 0;
				while (func[i] != ',')
				{
					if (func[i]<'0' || func[i]>'9')
						return false;
					getSelectedFile = getSelectedFile * 10 + func[i] - 48;
					n.push_back(getSelectedFile);
					i++;
				}
				i++;
			}
			if (func[i] != '\0')  return false;
			ifstream in(file, ios::binary);
			HUF info;
			in >> info;
			vector<HUFHeader> headers = info.getHeaders();
			vector<CharStat> stat = info.getCharStat();
			if (stat.size() > 0)
			{
				//tao cay huffman
				HuffNode* root = HuffmanTree(stat);
				int k = n.size();
				cout << "Bat dau giai nen tu file " << file << "...\n\n";
				for (int j = 0; j < k; j++)
				{
					int i = n[j];
					ofstream out(headers[i].fileName);
					in.seekg(headers[i].curFileAddrOffset);
					unsigned int Size = ExtractFile(out, in, root, headers[i].alignBits, headers[i].sizeAfter);
					bool check_sum = Checksum(Size, headers[i].sizeBefore);
					out.close();
					if (check_sum) cout << "[ OK ] Checksum file " << headers[i].fileName << "hop le.\n";
					else cout << "[ FAIL ] Checksum khong hop le. Giai nen file " << headers[i].fileName << "that bai!\n";
				}
			}
		}
		else {
			return false;
		}
	}
	return true;
}
