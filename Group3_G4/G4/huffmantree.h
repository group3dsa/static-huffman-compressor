﻿/**************************************************
Thư viện này chứa các struct và hàm liên quan đến
cây Huffman và mã hóa / giải mã Huffman.
**************************************************/

#ifndef __HUFFMANTREE_H__
#define __HUFFMANTREE_H__

#include "fileops.h"
#include "bitops.h"
#include <iostream>
#include <algorithm>

#define NUM_CHARS 256

typedef struct HuffNode {
	unsigned char c;
	long nFreq;
	int nLeft;
	int nRight;
};

/* Phần tử danh sách mã bit */
typedef struct {
	int length;     /* độ dài mã bit */
	unsigned char code[32];    /* mã bit của ký tự */
} symbol;

HuffNode* HuffmanTree(vector<CharStat> stat);

void generateCodeList(HuffNode * root, int cur, unsigned char tempcode[32], int top, symbol *code_list);

void printCodeList(symbol *code_list);

unsigned int CompressFile(ifstream& file, ofstream& fileCompress, HuffNode* root, char& Align, unsigned int size);

unsigned int ExtractFile(ofstream& file, ifstream& fileCompress, HuffNode* root, char Align, unsigned int size);

bool Checksum(unsigned int Size, unsigned int sizeBefore);

#endif
