﻿/**************************************************
Thư viện này chứa các struct, class và hàm liên quan
đến file nén HUF.
**************************************************/

#ifndef __HUFFILE_H__
#define __HUFFILE_H__

#include <string>
#include <iostream>
#include <vector>
#include "fileops.h"

using namespace std;

/***************************************************************
*	Tên lớp		:	HUFHeader
*	Chức năng	:	các thuộc tính và phương thức liên quan đến
*					header của định dạng file nén HUF
*	Người viết	:	Dũng
***************************************************************/
struct HUFHeader {
	string fileName;
	char   alignBits;
	unsigned int sizeBefore;
	unsigned int sizeAfter;
	unsigned int curFileAddrOffset;
	unsigned int nextFileAddrOffset;
	bool Input(istream& in);
	bool Output(ostream& out);
};

typedef struct HUFHeader HUFHeader;

//istream& operator>>(istream& in, HUFHeader& header);
//ostream& operator<<(ostream& out, HUFHeader& header);

/***************************************************************
*	Tên lớp		:	HUF
*	Chức năng	:	các thuộc tính và phương thức liên quan đến
*					file HUF
*	Người viết	:	Dũng
***************************************************************/
class HUF {
	char              m_CodeFile[2];
	int	              m_nCode;
	vector<CharStat>  m_CodeTable;
	vector<HUFHeader> m_FileHeader;

public:
	HUF(void);
	HUF(char* Code, int nCode, vector<CharStat> CodeTable);
	~HUF(void);

	bool isHUF();
	HUFHeader getHeader(int i);
	void insertHeader(HUFHeader t);
	vector<CharStat> getCharStat() { return m_CodeTable; }


	vector<HUFHeader> getHeaders() { return m_FileHeader; }

	friend istream& operator>>(istream& in, HUF& info);
	friend ostream& operator<<(ostream& out, HUF& info);
};

#endif
