﻿/**************************************************
Thư viện này chứa các struct và hàm liên quan đến
thao tác trên thư mục và file.
**************************************************/

#ifndef __FILEOPS_H__
#define __FILEOPS_H__

#include <windows.h>
#include <tchar.h> 
#include <strsafe.h>
#include <fstream>
#include <vector>

#include <iostream>
#include <string>
#pragma comment(lib, "User32.lib")
using namespace std;

#define MAX 128

struct CharStat
{
	unsigned char name;
	unsigned int frequency;
};

bool fileList(TCHAR* directory, vector<unsigned long>& bytes, vector<string>& filenames);

void CharFreq(vector<string> p, vector<CharStat>& table);

#endif