﻿/**************************************************
Thư viện này chứa các hàm dùng để thao tác với một 
dãy 256 bits tạo thành từ 32 unsigned char.
Tại sao lại là 256? Vì mã bit dài nhất của ký tự 
ít xuất hiện nhất có thể có độ dài n - 1 = 255 bit.
**************************************************/

#ifndef __BITOPS_H__
#define __BITOPS_H__

#include <string>

/* Mô tả chi tiết mỗi hàm vui lòng xem trong file .cpp */
void setBit(unsigned char *bits, unsigned char idx);
void clearBit(unsigned char *bits, unsigned char idx);
void clearBlock(unsigned char *bits);
int testBit(const unsigned char *bits, unsigned char idx);
void copyBlock(unsigned char *dest, const unsigned char *src);

#endif