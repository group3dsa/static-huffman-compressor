﻿#include "bitops.h"

/***************************************************************
*	Tên hàm		:	setBit
*	Chức năng	:	bật 1 bit bất kỳ trong mảng bit
*	Truyền vào	:	con trỏ tới mảng chứa các bit, vị trí bit 
*					cần bật
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void setBit(unsigned char *bits, unsigned char idx) {
	int byte;
	byte = idx / 8;        
	idx = idx % 8;         
	bits[byte] |= (0x80 >> idx);
}

/***************************************************************
*	Tên hàm		:	clearBit
*	Chức năng	:	tắt 1 bit bất kỳ trong mảng bit
*	Truyền vào	:	con trỏ tới mảng chứa các bit, vị trí bit
*					cần tắt
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void clearBit(unsigned char *bits, unsigned char idx) {
	int byte;
	unsigned char mask;
	byte = idx / 8; 
	idx = idx % 8;
	mask = (0x80 >> idx);
	mask = ~mask;
	bits[byte] &= (mask);
}
/***************************************************************
*	Tên hàm		:	clearBlock
*	Chức năng	:	clear toàn bộ bit trong một block (tức là
*					reset tất cả bit về 0). Lưu ý: hàm có sử
*					dụng memset.
*	Truyền vào	:	con trỏ tới mảng chứa các bit
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void clearBlock(unsigned char *bits) {
	memset((void *)bits, 0, 32);
}

/***************************************************************
*	Tên hàm		:	testBit
*	Chức năng	:	trả về giá trị của một bit trong block
*	Truyền vào	:	con trỏ tới mảng chứa các bit, vị trí bit cần
*					biết giá trị
*	Trả về		:	giá trị của bit được chỉ định
*	Người viết	:	Duy
***************************************************************/
int testBit(const unsigned char *bits, unsigned char idx) {
	int byte;
	unsigned char mask;
	byte = idx / 8;        
	idx = idx % 8;          
	mask = (0x80 >> idx);
	return((bits[byte] & mask) != 0);
}

/***************************************************************
*	Tên hàm		:	copyBlock
*	Chức năng	:	copy một block tới block khác
*	Truyền vào	:	con trỏ tới mảng chứa các bit cần copy, 
*					con trỏ tới mảng bit đích
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void copyBlock(unsigned char *dest, const unsigned char *src) {
	memcpy((void *)dest, (void *)src, 32);
}