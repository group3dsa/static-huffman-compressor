﻿#include "fileops.h"
#include "appops.h"
#include "huffile.h"

/***************************************************************
*	Tên hàm		:	main
*	Chức năng	:	đầu vào chương trình
*	Truyền vào	:	số lượng tham số dòng lệnh và danh sách các
*					tham số
*	Trả về		:	0 nếu chạy và thoát thành công, != 0 nếu
*					thất bại
*	Người viết	:	Dũng. Title ấn tượng là của Duy :3
***************************************************************/
int main(int argc, char ** argv)
{
	char* title =
		"\t\t%c          ______    ______  	      %c\n"
		"\t\t%c        .' ___  |  / ____ `.	      %c\n"
		"\t\t%c       / .'   \\_|  `'  __) |	      %c\n"
		"\t\t%c       | |   ____  _  |__ '.	      %c\n"
		"\t\t%c       \\ `.___]  || \\____) |	      %c\n"
		"\t\t%c        `._____.'  \\______.'	      %c\n"
		"\t\t%c			              %c\n"
		"\t\t%c GLORIOUS STATIC HUFFMAN CODING DEMO %c\n"
		"\t\t%c%";

	fprintf(stdout, title, 186, 186, 186, 186, 186, 186, 186,
		186, 186, 186, 186, 186, 186, 186, 186, 186, 200);
	char bottom_edge = 205;
	for (int i = 0; i < 37; i++) {
		fprintf(stdout, "%c", bottom_edge);
	}
	fprintf(stdout, "%c\n\n", 188);

	//char temp[] = "dic", temp2[] = "out\\out.huf", temp3[] = "-v", temp4[] = "-all";
	//argv[2] = temp2;
	//argv[3] = temp2;
	//argv[1] = temp3;

	if (argc >= 3) {
		if (argv[1][0] == '-'&&argv[1][2] == '\0')
		{
			cout << "Chuc nang duoc lua chon: ";
			switch (argv[1][1])
			{
			case 'e':
				cout << "nen file. Dang xu ly...\n\n";
				Encode(argv[2], argv[3]);
				break;
			case 'v':
				cout << "xem noi dung file nen. Dang doc file...\n\n";
				View(argv[2]);
				break;
			case 'd':
			{
				cout << "giai nen. Dang xu ly...\n";
				bool re = Decode(argv[2], argv[3]);
				//if (re) cout << "Giai nen thanh cong!\n";
				//else cout << "Giai nen khong thanh cong!\n";
				break;
			}
			default:
				break;
			}
			return 1;
		}
		else cout << "[ FAIL] Tham so dong lenh khong duoc ho tro!\n";
	}
	else cout << "[ FAIL] Cu phap tham so dong lenh khong chinh xac!\n";
	return 0;
}